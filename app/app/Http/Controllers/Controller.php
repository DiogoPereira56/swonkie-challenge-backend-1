<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\User;

class Controller extends BaseController
{
    public function add(Request $req)
    {
        include('simple_html_dom.php');
        
        $chanel_id= substr($req, -24);

        // Create DOM from URL
        $html = file_get_html('https://www.youtube.com/feeds/videos.xml?channel_id='.$chanel_id);

        // Finds all links
        $i=0;
        $array_url = [];
        foreach($html->find('link') as $link){
            if(strpos($link, "/watch?v="))
            {
                $array_url[$i] = $link->href;
                echo $array_url[$i] . '<br>';
                $i++;
            }
        }


        echo "<br>------------------------------<br><br>";

        // Finds all titles
        $i=0;
        $array_title = [];
        $primeira=TRUE;
        foreach($html->find('title') as $title){
            if(!$primeira){
                foreach($title->find('text') as $text){  
                    $array_title[$i] = $text;  
                    echo $array_title[$i];
                    $i++;
                }
            }else
                $primeira=FALSE;
            echo "<br>";
        }

        echo "<br>------------------------------<br><br>";

        // Finds all descriptions
        $i=0;
        $array_descriptions = [];
        foreach($html->find('media:description') as $description){
            foreach($description->find('text') as $text){
                $array_descriptions[$i] = $text;  
                echo $array_descriptions[$i];
                $i++;
            }
            echo "<br><br>";
        }

        echo "<br>------------------------------<br><br>";

        // Finds all dates of the videos
        $i=0;
        $array_dates = [];
        foreach($html->find('published') as $datePub){
            foreach($datePub->find('text') as $text){
                $array_dates[$i] = $text;  
                echo $array_dates[$i];
                $i++;
            }
            echo "<br>";
        }

        echo "<br>------------------------------<br><br>";
        
        //'id' => $id,
        for($x = 0; $x < $i-1; $x++)
        {
            User::create([
                'urls' => $array_url[$x],
                'titles' => $array_title[$x],
                'description' => $array_descriptions[$x],
                'date' => $array_dates[$x]
             ]);

        }
       
    }

    public function findAll(){
        return response()->json(User::all());
    }

    public function findId($id){
        return response()->json(User::find($id));
    }

    public function update($id, Request $req){
        $data= User::findOrFail($id);
        //$data->titles = $req->titles;
        //$data->description = $req->description;
        $data->update($req->all());
        return response()->json($data, 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted successfully', 200);
    }
}
