<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return "Hello from swk dev team";
    }
);

//This endpoint will receive a channel URL and scrape the videos to save in the database.
$router->post('videos', 'Controller@add');

//This endpoint will list all the videos that are saved in the database.
$router->get("videos", "Controller@findAll");

//This endpoint will list all the videos with that id that are saved in the database.
$router->get("video/{id}", "Controller@findId");

//This endpoint will delete the video from the database.
$router->delete("video/{id}", "Controller@delete");

//This endpoint will edit the video from the database. 
//The user should only be able to edit the title and description
$router->put("video/{id}", "Controller@update");

